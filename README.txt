Qlearning:
On the "qlearning" branch the qlearning algorithm is visiualized with 
path drawings and stats from the learning process.
In the file qlearning.cpp you can change vanlues in the "config" section 
to run the simulation with different parameters.
The code used for the qlearning experiment can be found in "qlearning_experiment"

Fuzzy Logic:
The code to the obstacle avoidance experiment is found on 
branch "fuzzylogic_avoidance_experiment"
The experiment in the "bigworld" environment is found on
branch "fuzzylogic_bigworld_experiment", this is also a good place to se a
demonstration of the fuzzy controller and pathfinding algoritms.

Robotics:
The implementation of roadmap and A* algorithm can be found on branch 
"pathfinding" in the files: "pathplanner.h" 
"pathplanner.cpp" "util.h" "util.cpp".

Computer Vision:
The implementaion of computer vision algorithms used for marble detection 
can be found on branch "marble_detection" in files "vision.cpp" and "vision.h"
A small demonstration of one of the method can be seen by running "main.cpp"
